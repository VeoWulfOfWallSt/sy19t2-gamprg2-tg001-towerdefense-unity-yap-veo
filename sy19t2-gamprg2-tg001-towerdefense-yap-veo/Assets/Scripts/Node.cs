﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Node : MonoBehaviour
{

    public Color hoverColor;
    private Color startColor;
    private Renderer render;

    public GameObject turret;
    public GameObject Ghost;


    

    
    void Start()
    {
        render = GetComponent<Renderer>();
        startColor = render.material.color;

    }
    void OnMouseEnter()
    {
     
            GameObject ghostObject = BuildManager.instance.GetGhostTurret();
            Ghost = (GameObject)Instantiate(ghostObject, transform.position, transform.rotation);
          
       
        

    }
    void OnMouseExit()
    {
        render.material.color = startColor;
        Destroy(Ghost);
    }
    void OnMouseDown()
    {
        if(turret != null)
        {
            Debug.Log("Node Occupied");
 
        }
        if(PlayerManager.playerInstance.playerGold>=25)
        {
            GameObject turretToPlace = BuildManager.instance.GetTurretToPlace();
            turret = (GameObject)Instantiate(turretToPlace, transform.position, transform.rotation);
            PlayerManager.playerInstance.playerGold -= 25;
            
        }
        



    }
}
