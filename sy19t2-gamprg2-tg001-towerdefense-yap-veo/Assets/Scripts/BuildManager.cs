﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{

    public static BuildManager instance;
    // Start is called before the first frame update
    private GameObject turretToPlace;
    private GameObject ghostTurret;

    public GameObject level1Turret;
    public GameObject ghostedTurret;


    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        turretToPlace = level1Turret;
        ghostTurret = ghostedTurret;
    }


    public GameObject GetTurretToPlace()
    {
        return turretToPlace;
    }

    public GameObject GetGhostTurret()
    {
        return ghostTurret;
    }



}
