﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager playerInstance;
    public Text Gold;
    public int playerGold;
    void Start()
    {
        playerInstance = this;
        playerGold = 125;
    }

    // Update is called once per frame
    void Update()
    {
        Gold.text = playerGold.ToString();
        if(playerGold<=0)
        {
            playerGold = 0;
        }
    }

   
    
}
