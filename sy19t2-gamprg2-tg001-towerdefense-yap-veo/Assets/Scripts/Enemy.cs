﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public static Enemy enemyInstance;

    public float health;
    public int goldDrop;
    public float speed;
    public int damage;
    

    void Start()
    {
        enemyInstance = this;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int dmg)
    {
        Debug.Log("Damaged");
        health -= dmg;

        if (health <= 0)
        {
            Death();
        }
    }
    public void Death()
    {
        PlayerManager.playerInstance.playerGold += goldDrop;
        Destroy(gameObject);
    }

    
}
