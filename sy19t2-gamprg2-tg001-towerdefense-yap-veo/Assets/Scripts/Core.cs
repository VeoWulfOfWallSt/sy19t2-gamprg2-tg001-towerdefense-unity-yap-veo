﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Core : MonoBehaviour
{
    public static Core coreInstance;
    public int coreHealth1;
    public int coreHealth2;
    public Text health1Txt;
    public Text health2Txt;
    public GameObject CoreTop;
    public GameObject CoreBottom;
    void Start()
    {
        coreInstance = this;
        coreHealth1 = 100;
        coreHealth2 = 100;
    }

    // Update is called once per frame
    void Update()
    {
        health1Txt.text = coreHealth1.ToString();
        health2Txt.text = coreHealth2.ToString();
        if(coreHealth1 <0)
        {
            Destroy(CoreTop);
            coreHealth1 = 0;
        }
        if (coreHealth2 < 0)
        {
            Destroy(CoreBottom);
            coreHealth2 = 0;
        }
        if((coreHealth1<=0)&&(coreHealth2<=0))
        {
            SceneManager.LoadScene(2);
        }
        
    }


}
