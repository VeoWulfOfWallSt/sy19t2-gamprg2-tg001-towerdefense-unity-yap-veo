﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBehavior : MonoBehaviour
{
    public Transform target;

    private Enemy targetEnemy;


    public float range;

   
    public GameObject bulletInstance;
    public float fireRate = 1.0f;
    private float shootNext = 0f;


    public Transform rotator;
    public float turnSpeed = 6.0f;

    public Transform bulletSpawn;

    void TargetUpdate()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            target = null;
        }

    }

    void Start()
    {
        InvokeRepeating("TargetUpdate", 0f, 0.6f);

    }

    // Update is called once per frame
    void Update()
    {
        LockOnTarget();

            if (shootNext <= 0f)
            {
                Shoot();
                shootNext = 1f / fireRate;
            }

            shootNext -= Time.deltaTime;

    }
    void LockOnTarget()
    {

        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(rotator.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
        rotator.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }
    void Shoot()
    {
        GameObject firedBullet = (GameObject)Instantiate(bulletInstance, bulletSpawn.position, bulletSpawn.rotation);
        BulletBehavior bullet = firedBullet.GetComponent<BulletBehavior>();

        if (bullet != null)
            bullet.Seek(target);
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

