﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Transform target;
    private int index = 0;
    public List<GameObject> points;
    public string waypointTags;


    void Start()
    {
        points = new List<GameObject>();
        points.AddRange(GameObject.FindGameObjectsWithTag(waypointTags));
        target = points[0].transform;
    }

    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * Enemy.enemyInstance.speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }

        
    }

    void GetNextWaypoint()
    {
        if (index >= points.Count - 1)
        {
            if(waypointTags == "Bottom Points")
            {
                Core.coreInstance.coreHealth2 -= 10;
            }
            if (waypointTags == "Top Points")
            {
                Core.coreInstance.coreHealth1 -= 10;
            }
            Destroy(gameObject);
            return;
        }

        index++;
        target = points[index].transform;
    }

    


}
